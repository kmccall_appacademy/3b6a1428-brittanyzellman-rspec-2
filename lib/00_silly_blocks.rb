require "byebug"

def reverser
  arr = yield.split(" ")
  arr.map { |word| word.reverse! }
  arr.join(" ")
end

def adder(num = 1)
  yield + num
end

def repeater(n = 1)
    n.times{|n| yield }
end
