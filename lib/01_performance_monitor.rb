require "byebug"

def measure(n = 1)
  t = Time.now
  if n == 1
    yield
    return Time.now - t
  else
    # debugger
    num = 0
    n.times { num = yield }
    if num.is_a? Integer
      return num
    else
      x = (t - num).abs
      return x / n
    end
  end
end
